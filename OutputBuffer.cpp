/*
 * Copyright (C) 2017 Konsulko Group
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <OutputBuffer.h>

OutputBuffer::OutputBuffer(const QAudioFormat &format, QObject *parent)
  : QIODevice(parent)
{
    m_sample_size = format.bytesPerFrame();
}

OutputBuffer::~OutputBuffer()
{
}

void OutputBuffer::start()
{
    open(QIODevice::ReadWrite);
}

void OutputBuffer::stop()
{
    close();
    QMutexLocker locker(&m_mutex);
    m_buffer.clear();
}

qint64 OutputBuffer::readData(char *data, qint64 len)
{
    QMutexLocker locker(&m_mutex);
    int n = qMin((qint64) m_buffer.size(), len);

    // Make sure reads are in units of the sample size
    n = n - (n % m_sample_size);

    if (!m_buffer.isEmpty()) {
        memcpy(data, m_buffer.constData(), n);
	m_buffer.remove(0, n);
    }
    return n;
}

qint64 OutputBuffer::writeData(const char *data, qint64 len)
{
    QMutexLocker locker(&m_mutex);
    m_buffer.append(data, len);
    return len;
}

qint64 OutputBuffer::bytesAvailable()
{
    QMutexLocker locker(&m_mutex);
    return m_buffer.size() + QIODevice::bytesAvailable();
}
