/*
 * Copyright (C) 2017 Konsulko Group
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Simple buffer device to act as input device for QAudioInput.
 *
 * It basically wraps QByteArray with some additional locking that
 * the QBuffer class does not provide.  It also ensures that reads
 * return in units of the given format's sample size.  This is
 * required to handle the RTL-SDR library output sometimes arriving
 * with half a sample at the end.  QAudioOutput will blindly pass
 * that half sample along and trigger a PulseAudio error if we do
 * not handle it here.
 */

#ifndef OUTPUTBUFFER_H
#define OUTPUTBUFFER_H

#include <QtCore/QIODevice>
#include <QtCore/QMutex>
#include <QtMultimedia/QAudioFormat>

class OutputBuffer : public QIODevice
{
    Q_OBJECT

public:
    OutputBuffer(const QAudioFormat &format, QObject *parent);
    ~OutputBuffer();

    void start();
    void stop();

    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);
    qint64 bytesAvailable();

private:
    QMutex m_mutex;
    QByteArray m_buffer;
    unsigned int m_sample_size;
};

#endif // OUTPUTBUFFER_H
