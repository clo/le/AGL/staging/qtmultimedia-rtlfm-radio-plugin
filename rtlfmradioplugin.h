/* Copyright (C) 2016, The Qt Company Ltd. All Rights Reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef RTLFMRADIOPLUGIN_H
#define RTLFMRADIOPLUGIN_H

#include <QMediaServiceProviderPlugin>

class RtlFmRadioPlugin : public QMediaServiceProviderPlugin, public QMediaServiceSupportedDevicesInterface
{
    Q_OBJECT
    Q_INTERFACES(QMediaServiceProviderFactoryInterface)
    Q_INTERFACES(QMediaServiceSupportedDevicesInterface)
    Q_PLUGIN_METADATA(IID QMediaServiceProviderFactoryInterface_iid FILE "rtlfmradio.json")

public:
    QMediaService* create(QString const& key);
    void release(QMediaService *service);

    QList<QByteArray> devices(const QByteArray &service) const;
    QString deviceDescription(const QByteArray &service, const QByteArray &device);
};

#endif // RTLFMRADIOPLUGIN_H
