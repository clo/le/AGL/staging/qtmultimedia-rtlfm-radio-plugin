/* Copyright (C) 2016, The Qt Company Ltd. All Rights Reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef RTLFMRADIOSERVICE_H
#define RTLFMRADIOSERVICE_H

#include <QtMultimedia/QMediaService>

class QRadioTunerControl;

class RtlFmRadioService : public QMediaService
{
    Q_OBJECT
public:
    RtlFmRadioService(QObject *parent = 0);
    ~RtlFmRadioService();

    QMediaControl *requestControl(const char *name);
    void releaseControl(QMediaControl *);

private:
    QRadioTunerControl *m_control;
};

#endif // RTLFMRADIOSERVICE_H
