/* Copyright (C) 2016, The Qt Company Ltd. All Rights Reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "rtlfmradioservice.h"
#include "rtlfmradiotunercontrol.h"

RtlFmRadioService::RtlFmRadioService(QObject *parent)
    : QMediaService(parent)
    , m_control(new RtlFmRadioTunerControl(this))
{
}

RtlFmRadioService::~RtlFmRadioService()
{
}

QMediaControl *RtlFmRadioService::requestControl(const char *name)
{
    if (qstrcmp(name,QRadioTunerControl_iid) == 0)
        return m_control;

    return 0;
}

void RtlFmRadioService::releaseControl(QMediaControl *control)
{
    delete control;
    m_control = 0;
}
