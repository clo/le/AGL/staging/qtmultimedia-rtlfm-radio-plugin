/* Copyright (C) 2016, The Qt Company Ltd. All Rights Reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "rtlfmradioplugin.h"
#include "rtlfmradioservice.h"

QMediaService* RtlFmRadioPlugin::create(QString const& key)
{
    if (key == QLatin1String(Q_MEDIASERVICE_RADIO))
        return new RtlFmRadioService;

    return 0;
}

void RtlFmRadioPlugin::release(QMediaService *service)
{
    delete service;
}

QList<QByteArray> RtlFmRadioPlugin::devices(const QByteArray &service) const
{
    Q_UNUSED(service)
    return QList<QByteArray>();
}

QString RtlFmRadioPlugin::deviceDescription(const QByteArray &service, const QByteArray &device)
{
    Q_UNUSED(service)
    Q_UNUSED(device)
    return QString();
}
