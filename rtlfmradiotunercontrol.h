/* Copyright (C) 2016, The Qt Company Ltd. All Rights Reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef RTLFMRADIOTUNERCONTROL_H
#define RTLFMRADIOTUNERCONTROL_H

#include <QtMultimedia/QRadioTunerControl>

class RtlFmRadioTunerControl : public QRadioTunerControl
{
    Q_OBJECT
public:
    RtlFmRadioTunerControl(QObject *parent = 0);
    ~RtlFmRadioTunerControl();

    QRadioTuner::State state() const;

    QRadioTuner::Band band() const;
    void setBand(QRadioTuner::Band band);
    bool isBandSupported(QRadioTuner::Band band) const;

    int frequency() const;
    int frequencyStep(QRadioTuner::Band band) const;
    QPair<int,int> frequencyRange(QRadioTuner::Band band) const;
    void setFrequency(int frequency);

    bool isStereo() const;
    QRadioTuner::StereoMode stereoMode() const;
    void setStereoMode(QRadioTuner::StereoMode stereoMode);

    int signalStrength() const;

    int volume() const;
    void setVolume(int volume);

    bool isMuted() const;
    void setMuted(bool muted);

    bool isSearching() const;

    bool isAntennaConnected() const { return true; }

    void searchForward();
    void searchBackward();
    void searchAllStations(QRadioTuner::SearchMode searchMode = QRadioTuner::SearchFast);
    void cancelSearch();
private:
    void setSearching(bool searching);

public:
    void start();
    void stop();

    QRadioTuner::Error error() const;
    QString errorString() const;

private:
    void setError(QRadioTuner::Error error, const QString &errorString);

private:
    class Private;
    Private *d;
};

#endif // RTLFMRADIOTUNERCONTROL_H
