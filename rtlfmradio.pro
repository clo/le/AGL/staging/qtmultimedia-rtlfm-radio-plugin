TEMPLATE = lib
CONFIG += plugin c++11 link_pkgconfig
PKGCONFIG += librtlsdr
TARGET = rtlfmradio
QT = multimedia

HEADERS = rtlfmradioplugin.h rtlfmradioservice.h rtlfmradiotunercontrol.h OutputBuffer.h rtl_fm.h convenience/convenience.h
SOURCES = rtlfmradioplugin.cpp rtlfmradioservice.cpp rtlfmradiotunercontrol.cpp OutputBuffer.cpp rtl_fm.c convenience/convenience.c
DISTFILES += rtlfmradio.json

target.path = $$[QT_INSTALL_PLUGINS]/mediaservice
INSTALLS += target
